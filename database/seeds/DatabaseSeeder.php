<?php

use App\Lesson;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lesson::truncate();
        $this->call('LessonsTableSeeder');
        // $this->call(UserTableSeeder::class);
    }
}
