<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Acme\Transformers\LessonTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LessonsController extends Controller
{
    protected $lessonTransformer;

    function __construct(LessonTransformer $lessonTransformer) {
        $this->lessonTransformer = $lessonTransformer;
    }


    public function index(){
        
        // 1. All is bad
        // 2. No way to attach meta data
        // 3. Linking db structure to api output
        // 4. No way to signal header/response code
        
        $lessons = Lesson::all();   // bad practice
        
        return response()->json([
            'data' => $this->lessonTransformer->transformCollection($lessons->all())
        ], 200);
        
    }
    
    public function show($id){
        $lesson = Lesson::find($id);
        
        if(!$lesson){
            return response()->json([
                'error' => [
                    'message' => 'Lesson does not exist'
                ]
            ], 404);
        }
        
        return response()->json([
            'data' => $this->lessonTransformer->transform($lesson)
        ], 200);
    }
}
