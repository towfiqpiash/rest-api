<?php

namespace App\Acme\Transformers;

/**
 * Description of LessonTransformer
 *
 * @author piash
 */
class LessonTransformer extends Transformer{
    
    public function transform($lesson) {
        return [
            'title' => $lesson['title'],
            'body' => $lesson['body'],
            'active' => (bool) $lesson['some_bool']
        ];
    }

}
