<?php

namespace App\Acme\Transformers;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transformer
 *
 * @author piash
 */
abstract class Transformer {
    
    public function transformCollection(array $items){
        return array_map([$this, 'transform'], $items);
    }
    
    public abstract function transform($item);
}
